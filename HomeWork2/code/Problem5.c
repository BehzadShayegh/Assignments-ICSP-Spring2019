#include <stdio.h>

int main() {
    int number;
    scanf("%d", &number);
    while(number != 0) {
        int sum = 0 , n = number;
        while(n > 0) {
            sum += n%10;
            n /= 10;
        }
        printf("%d\n",sum);
        scanf("%d", &number);
    }
    return 0;
}