#include <stdio.h>
#include <math.h>

int main() {
    int number;
    scanf("%d", &number);
    
    for (int i = 0; i < number; i++) {
        for(int j = 0; j < abs(number/2-i); j++)
            printf(" ");
        for(int j = 0; j < number-2*abs(number/2-i); j++)
            printf("*");
        printf("\n");
    }
    return 0;
}