#include <stdio.h>
#include <stdlib.h>

void grad(int* A, int k) {
    for(int i=0; i<k-1; i++)
        A[i] = A[i+1]-A[i];
}

int main() {
    int k;
    scanf("%d", &k);

    int n;
    scanf("%d", &n);
    
    int* A = (int *)malloc(k*sizeof(int));
    for(int i=0; i<k; i++)
        scanf("%d", &A[i]);

    for(int i=0; i<n; i++)
        grad(A, k-i);

    for(int i=0; i<k-n; i++)
        printf("%d ", A[i]);
}