#include <stdio.h>
#include <stdlib.h>

int Binary_Search(int* L, int first, int last, int target) {
    if(first == last) return first;
    if(first == last-1) return first;
    int mid = (first + last)/2;
    if(target > L[mid]) return Binary_Search(L, mid, last, target);
    else return Binary_Search(L, first, mid, target);
}

int main() {
    int n;
    scanf("%d", &n);
    
    int* L = (int *)malloc((n+1)*sizeof(int));
    L[0] = 0;
    for(int i=1; i<=n; i++)
        scanf("%d", &L[i]);

    int k;
    scanf("%d", &k);
    
    int* A = (int *)malloc(k*sizeof(int));
    for(int i=0; i<k; i++)
        scanf("%d", &A[i]);

    // L[0] += 1;
    for(int i=1; i<n; i++)
        L[i] += L[i-1];

    for(int i=0; i<k; i++)
        printf("%d\n", Binary_Search(L, 0, n, A[i])+1);
}