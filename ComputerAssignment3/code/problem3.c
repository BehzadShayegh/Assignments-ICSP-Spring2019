#include <stdio.h>
#include <stdlib.h>

#define True 1
#define False 0

int relax(int** A, int i, int j) {
    if(A[i][j] > A[i][j+1] && A[i][j] > A[i][j-1]
    && A[i][j] < A[i+1][j] && A[i][j] < A[i-1][j])
        return True;
        
    if(A[i][j] < A[i][j+1] && A[i][j] < A[i][j-1]
    && A[i][j] > A[i+1][j] && A[i][j] > A[i-1][j])
        return True;

    return False;
}

int main() {
    int n;
    scanf("%d", &n);

    int m;
    scanf("%d", &m);
    
    int** A = (int **)malloc(n*sizeof(int *));
    for(int i=0; i<n; i++)
        A[i] = (int *)malloc(m*sizeof(int));

    for(int i=0; i<n; i++)
        for(int j=0; j<m; j++)
            scanf("%d", &A[i][j]);

    int count = 0;
    for(int i=1; i<n-1; i++)
        for(int j=1; j<m-1; j++)
            if(relax(A, i, j))
                count++;
    
    printf("%d", count);
}