#include <stdio.h>

void swap(int* array, int index1, int index2) {
        int temp = array[index1];
        array[index1]   = array[index2];
        array[index2] = temp;
}

void BubbleSort(int* array, int size) {
    for (int i = 0 ; i < size-1; i++)
        for (int j = 0 ; j < size-1-i; j++)
            if (array[j] > array[j+1])
                swap(array, j, j+1);
}

int main()
{
    const int size = 5;
    int array[size];

    for (int i = 0; i < size; i++)
    scanf("%d", &array[i]);

    BubbleSort(array, size);

    for (int i = 0; i < size; i++)
        printf("%d ", array[i]);

    return 0;
}