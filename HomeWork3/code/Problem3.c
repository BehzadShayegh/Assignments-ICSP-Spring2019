#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 

void swap(char** strings, int index1, int index2) {
        char* temp = strings[index1];
        strings[index1] = strings[index2];
        strings[index2] = temp;
}

void BubbleSort(char** strings, int size) {
    for (int i = 0 ; i < size-1; i++)
        for (int j = 0 ; j < size-1-i; j++)
            if (strcmp(strings[j], strings[j+1]) > 0)
                swap(strings, j, j+1);
}

int main() { 
    int size;
    scanf("%d", &size);

    char* strings[size];
    for(int i = 0; i < size; i++)
        strings[i] = (char *)malloc(100);

    gets();
    for (int i = 0; i < size; i++)
        gets(strings[i]);

    BubbleSort(strings, size);
    
    printf("----------------------------\n");
    for (int i = 0; i < size; i++)
        printf("%s\n", strings[i]);

    return 0;
} 