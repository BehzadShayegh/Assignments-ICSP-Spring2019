#include <stdio.h> 
#include <stdlib.h> 

const int size = 6;

int main() { 
    int A[size];
    for(int i=0; i<size; i++)
        scanf("%d", &A[i]);
    int k;
    scanf("%d", &k);
    
    int flag = 0;
    for(int i=0; i<size-1; i++)
        for(int j=i+1; j<size; j++)
            if(A[i]+A[j] == k) {
                flag = 1;
                break;
            }
    
    if(flag)
        printf("TRUE");
    else printf("FALSE");
}