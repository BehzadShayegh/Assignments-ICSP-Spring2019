#include <stdio.h> 
#include <stdlib.h> 

const int size = 8;

struct Node {
    int value;
    struct Node* next;
};
struct Node* newNode() {
    struct Node* node = (struct Node*)malloc(sizeof(struct Node));
    node->value = 0;
    node->next = NULL;
    return node;
}


int main() { 
    struct Node* A = newNode();
    struct Node* node = A;

    for(int i=0; i<size; i++) {
        scanf("%d", &node->value);
        node->next = newNode();
        node = node->next;
    }
    node = NULL;

    struct Node* prev = A;
    node = prev->next;
    while(node != NULL) {
        if(prev->value == node->value) {
            prev->next = node->next;
            node = prev->next;
        } else {
            prev = node;
            node = prev->next;
        }
    }

    node = A;
    while(node->next != NULL) {
        printf("%d", node->value);
        node = node->next;
    }
}