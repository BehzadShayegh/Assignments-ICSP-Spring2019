#include <stdio.h> 
#include <stdlib.h> 

const int size = 4;

struct Person {
    int number;
    char name[10];
    char lastName[10];
    int exp;
};
struct Person newPerson(int number, char* name, char* lastName, int exp) {
    struct Person person;
    person.number = number;
    for(int i=0; i<10; i++) {
        person.name[i] = name[i];
        person.lastName[i] = lastName[i];
    }
    person.exp = exp;
    return person;
}

int maxIndex(struct Person* persons, int size) {
    int max = 0;
    for(int i=0; i<size; i++)
        if(persons[i].exp > persons[max].exp)
            max = i;
    return max;
}

void swap(struct Person* persons, int l, int r) {
    struct Person temp = persons[l];
    persons[l] = persons[r];
    persons[r] = temp;
}

void sort(struct Person* persons, int size) {
    for(int i=0; i<size-1; i++) {
        int max = maxIndex(persons, size-i);
        if(max == size-i-1) continue;
        swap(persons, max, size-i-1);
    }
}

int main() { 
    struct Person persons[size];
    for(int i=0; i<size; i++) {
        int number;
        char name[10];
        char lastName[10];
        int exp;
        scanf("%d %s %s %d", &number, name, lastName, &exp);
        persons[i] = newPerson(number, name, lastName, exp);
    }

    sort(persons, size);

    for(int i=0; i<size; i++) {
        printf("%d %s %s %d\n", persons[i].number, persons[i].name, persons[i].lastName, persons[i].exp);
    }
}