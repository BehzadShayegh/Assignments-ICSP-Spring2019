#include <stdio.h> 
#include <stdlib.h> 

const int size = 6;

int maxIndex(int* array, int size) {
    int max = 0;
    for(int i=0; i<size; i++)
        if(array[i] > array[max])
            max = i;
    return max;
}

void swap(int* array, int l, int r) {
    int temp = array[l];
    array[l] = array[r];
    array[r] = temp;
}

int insertionSort(int* array, int size) {
    int steps = 0;
    for(int i=0; i<size-1; i++) {
        int max = maxIndex(array, size-i);
        if(max == size-i-1) continue;
        swap(array, max, size-i-1);
        steps++;
    }
    return steps;
}

int main() { 
    int A[size];
    for(int i=0; i<size; i++)
        scanf("%d", &A[i]);
    int k;
    
    printf("%d", insertionSort(A, size));
}