#include <stdio.h> 
#include <stdlib.h> 

const int size = 6;

struct Node {
    int value;
    struct Node* next;
};
struct Node* newNode() {
    struct Node* node = (struct Node*)malloc(sizeof(struct Node));
    node->value = 0;
    node->next = NULL;
    return node;
}

struct Node* cutLast(struct Node* list) {
    struct Node* node = list;
    struct Node* prev = NULL;
    while(node->next->next != NULL) {
        prev = node;
        node = prev->next;
    }
    prev->next = NULL;
    return node;
}

struct Node* rout(struct Node* list) {
    struct Node* first = list;
    struct Node* last = cutLast(list);
    list = last;
    last = NULL;
    list->next = first;
    return list;
}

int main() { 
    struct Node* A = newNode();
    struct Node* node = A;

    for(int i=0; i<size; i++) {
        scanf("%d", &node->value);
        node->next = newNode();
        node = node->next;
    }
    node = NULL;

    A = rout(A);

    node = A;
    while(node != NULL) {
        printf("%d", node->value);
        node = node->next;
    }
}